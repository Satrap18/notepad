#-------------------Importing-----------------------------#
from tkinter import *
from tkinter import filedialog
from tkinter import messagebox
from tkinter import colorchooser
from tkinter import ttk
from tkinter.ttk import *
import os
from datetime import datetime
#---------------------------------------------------------#
window = Tk()
#---------------------------------------------------------#
def lightgreen():
    text.config(bg='lightgreen')
def lightyellow():
    text.config(bg='lightyellow')
def lightblue():
    text.config(bg='lightblue')
def darkblue():
    text.config(bg='darkblue')
def black():
    text.config(bg='black')
def yellow():
    text.config(bg='yellow')
def purple():
    text.config(bg='purple')
def green():
    text.config(bg='green')
def white():
    text.config(bg='white')
def lightplus():
    text.config(bg='#e0e0e0')
def Monokai():
    text.config(bg='#2d2d2d')


def change_color():
    color = colorchooser.askcolor()
    text.configure(fg=color[1])

def new(envert=None):
    #text.delete(1.0,END)
    if messagebox.askyesnocancel("Notepad-Satrap18","Do you want to save changes to Untitled?"):
        Save()
    else:
        text.delete(1.0,END)
def Open(envert=None):
        global filename
        filename = filedialog.askopenfilename(defaultextension=".txt",title="OpenFile-Satrap",filetypes=[("All Files", "*.*"), ("Text Documents", "*.txt"),("All File",".*"),("HTML File",".html"),("txt File",".txt")])
        if filename == "":
            filename = None
        else:
            window.title(os.path.basename(filename) + "-Satrap")
            text.delete(1.0, END)
            fh = open(filename, "r")
            text.insert(1.0, fh.read())
            fh.close()


    
def Save(envert=None):
    global filename
    try:
        f = open(filename, 'w')
        letter = text.get(1.0, 'end')
        f.write(letter)
        f.close()
    except:
        save_as()

def save_as(envert=None):
    try:

        f = filedialog.asksaveasfilename(initialfile='Untitled.txt', defaultextension=".txt",title="SaveFile-Satrap",filetypes=[("All Files", "*.*"), ("Text Documents", "*.txt"),("HTML File",".html"),("txt File",".txt")])
        fh = open(f, 'w')
        global filename
        filename = f
        textoutput = text.get(1.0, END)
        fh.write(textoutput)
        fh.close()
        window.title(os.path.basename(f) + "Satrap18")
    except:
        pass


def Exit(envert=None):
    if messagebox.askokcancel("Exit", "Do you really want to Exit?"):
        window.destroy()
window.protocol('WM_DELETE_WINDOW',Exit)

def About(envert=None):
    messagebox.showinfo("Author","Satrap18\nversion 0.4")

def Format():
    Toplevel = Tk()
    Toplevel.title("Font")
    Toplevel.geometry('200x100')
    Toplevel.resizable(False,False)
    def Font():
        text.config(font=String.get())
        Toplevel.destroy()
    option = [
        "Font",
        "Arial",
        'Symbol',
        'MSSerif',
        'Terminal',
        'Constantia',
        "System",
        'Roman',
        'Wingdings',
        'Webdings',
        ]
        #Toplevel.destroy()
    String = StringVar()
    tt = OptionMenu(Toplevel,String,*option)
    tt.pack()
    String.set("Font")
    Button(Toplevel,text='Ok',command=Font).pack()
    Toplevel.mainloop()

def Cut(envert=None):
    text.event_generate("<<Cut>>")
def Copy(envert=None):
    text.event_generate("<<Copy>>")
def Paste(envert=None):
    text.event_generate("<<Paste>>")
def Select_All(envert=None):
    text.tag_add('sel','1.0','end')
def Time(envert=None):
    #current date and time
    now = datetime.now()
    #time format: M:S
    format = "%H:%M:%S %p %d/%m/%Y"
    #format datetime using strftime()
    time1 = now.strftime(format)
    text.insert(END,time1)
#--------------------------------Page---------------------#
window.title('Notepad-Satrap18')
window.geometry('900x500')
#----------------Create Text------------------------------#
text = Text(window,font=(20))
text.pack(expand=True,fill='both')
#---------------------------------------------------------#
scroll = Scrollbar(text)
text.configure(yscrollcommand=scroll.set)
scroll.config(command=text.yview)
scroll.pack(side=RIGHT, fill=Y)
#------------------Create menu----------------------------#
menubar = Menu(window)
#-----------------filemenu--------------------------------#
filemenu = Menu(menubar,tearoff=0)

menubar.add_cascade(label='File',menu = filemenu)
filemenu.add_command(label='New',accelerator='Ctrl+N',command=new)
filemenu.add_command(label='Open',accelerator='Ctrl+O',command=Open)
filemenu.add_command(label='Save',accelerator='Ctrl+S',command=Save)
filemenu.add_command(label='Save as',command=Save)
filemenu.add_command(label='Exit',accelerator='Alt+F4',command=Exit)
#---------------------------edit really-----------------------------#
edit2 = Menu(menubar,tearoff=0)

menubar.add_cascade(label='Edit',menu=edit2)
edit2.add_command(label='Cut',command=Cut,accelerator='Ctrl+X')
edit2.add_command(label='Copy',command=Copy,accelerator='Ctrl+C')
edit2.add_command(label='Paste',command=Paste,accelerator='Ctrl+V')
edit2.add_command(label='Select All',command=Select_All,accelerator='Ctrl+A')
edit2.add_command(label='Time/Date',command=Time,accelerator='F5')
#--------------------Edit--------------------------------------#
edit = Menu(menubar,tearoff=0)

menubar.add_cascade(label='ColorTheme',menu=edit)
edit.add_command(label='lightgreen',command=lightgreen)
edit.add_command(label='lightyellow',command=lightyellow)
edit.add_command(label='lightblue',command=lightblue)
edit.add_command(label='darkblue',command=darkblue)
edit.add_command(label='black',command=black)
edit.add_command(label='yellow',command=yellow)
edit.add_command(label='purple',command=purple)
edit.add_command(label='green',command=green)
edit.add_command(label='white',command=white)
edit.add_command(label='lightplus',command=lightplus)
edit.add_command(label='Monokai',command=Monokai)
#-----------------color font-------------------------------#
color = Menu(menubar,tearoff=0)

menubar.add_cascade(label='ColorFont',menu=color,command=change_color)
color.add_command(label="color",command=change_color)
#----------------formatmenu--------------------------------#
formatmenu = Menu(menubar,tearoff=0)

menubar.add_cascade(label='Format',menu=formatmenu)
formatmenu.add_command(label='Font...',command=Format)
#----------------------------------------------------------#
#------------------help menu-------------------------------#
helpmenu = Menu(menubar,tearoff=0)

menubar.add_cascade(label='Help',menu=helpmenu)
helpmenu.add_command(label='About Notepad',command=About,accelerator='Ctrl+H')
#-----------------------------------------------------------#
window.config(menu=menubar)
#-----------------------------------------------------------#
text.bind('<Control-N>', new)
text.bind('<Control-n>', new)
text.bind('<Control-O>', Open)
text.bind('<Control-o>', Open)
text.bind('<Control-S>', save_as)
text.bind('<Control-s>', save_as)
text.bind('<Control-H>', About)
text.bind('<Control-h>', About)
text.bind('<F5>', Time)
#----------------------------------------------------------------#
window.mainloop()
